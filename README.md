# Docker RoboHydra

Unofficial Docker image to run robohydra | robohydra.org

## How to use

```
$ docker run --rm \
    -v ./robohydra:/robohydra \
    -e PLUGINS='plugin1,plugin2' \
    -p 3000:3000
    inakimalerba/robohydra
```

where:

```
$ tree ./robohydra
robohydra/
├── plugin1
│   └── index.js
└── plugin2
    └── index.js
```

That's it. Robohydra is listening on port 3000.

To learn how to use Robohydra, please go to the [docs](http://robohydra.org/docs/0.6.8/tutorial/)
