FROM debian:unstable

ENV PATH $PATH:/node_modules/.bin

RUN apt-get update && apt-get install -y --no-install-recommends \
    eatmydata \
    && rm -rf /var/lib/apt

RUN apt-get update && eatmydata apt-get install -y --no-install-recommends \
    npm \
    && rm -rf /var/lib/apt

RUN npm install robohydra -y

COPY ./entrypoint.sh /entrypoint.sh

ENTRYPOINT './entrypoint.sh'
